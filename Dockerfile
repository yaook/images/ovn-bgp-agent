##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
FROM registry.yaook.cloud/yaook/neutron-ovn-agent-zed:1.0.75

COPY files/* /

RUN set -eux ; \
    export LANG=C.UTF-8 ; \
    export BUILD_PACKAGES="git patch python3-pip" ; \
    export REQUIRED_PACKAGES="frr sudo prometheus-frr-exporter" ; \
    useradd syslog ; \
    apt-get update ; \
    apt-get install -y ${BUILD_PACKAGES} ${REQUIRED_PACKAGES} ; \
    git clone https://opendev.org/openstack/ovn-bgp-agent.git --depth 1 --branch 3.0.0 ; \
    pip3 install ovn-bgp-agent/ ; \
    patch -p1 -d /usr/local/lib/python3.11/site-packages/ < /eventlet-fix-leak-of-epoll-file-descriptors.patch ; \
    patch -p1 -d /usr/local/lib/python3.11/site-packages/ < /fix-use-correct-delete-functions.patch ; \
    rm -rf requirements ovn-bgp-agent; \
    apt-get purge --auto-remove -y ${BUILD_PACKAGES} ; \
    rm -rf /var/lib/apt/lists/*

USER root

CMD ["ovn-bgp-agent"]
