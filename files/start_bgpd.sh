#!/bin/bash
##
## Copyright (c) 2022 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

set -euo pipefail

# shellcheck disable=SC1091
. /etc/ovn-bgp-agent/bridge_interface_mapping

LISTENING_IP=$(echo "${BGP_LOCAL_PEER_IP}" | cut -d '/' -f1)

/usr/lib/frr/bgpd -F traditional -A 127.0.0.1 --no_kernel -f /etc/frr/frr.conf -l "${LISTENING_IP}"
